document.addEventListener("DOMContentLoaded", function () {
	// alert("loaded");
	var vpanel = document.getElementById("aaversionpanel");
	if (!vpanel) {
		vpanel = document.createElement("div");
		vpanel.setAttribute("id", "aaversionpanel");
		document.body.appendChild(vpanel);
		vpanel.style.position = "fixed";
		vpanel.style.fontSize = "12px";
		vpanel.style.fontFamily = "monospace";
		vpanel.style.top = "10px";
		vpanel.style.right = "10px";
		vpanel.style.background = "#BBF";
		vpanel.style.padding = "10px";
		// vpanel.style.paddingLeft = "20px";
	}
	function _make(elt, parent) {
		var ret = document.createElement(elt);
		if (parent) {
			parent.appendChild(ret);
		}
		return ret;
	}
	function append_link(list, href, label) {
		var li = document.createElement("li"),
			a;

		if (window.location.href == href) {
			a = document.createElement("b");
			a.classList.add("self");
		} else {
			a = document.createElement("a");
			a.href = href;
		}
		a.innerHTML = label;
		li.appendChild(a);
		list.appendChild(li);
	}

	aaversionpanel.innerHTML = "";

	var p1 = _make("p", vpanel),
		contents = _make("div", vpanel),
		searchlinks = _make("ul", contents),
		p = _make("p", contents),
		versionlinks = _make("ul", contents);

	p1.innerHTML = "Links";
	p1.style.margin = "0";
	contents.style.marginTop = "0.5em";
	searchlinks.style.margin = "0";
	searchlinks.style.listStyle = "square";
	versionlinks.style.margin = "0";
	versionlinks.style.listStyle = "square";
	searchlinks.style.paddingLeft = "0";
	versionlinks.style.paddingLeft = "0";

	vpanel.addEventListener("touchstart", function () {
		contents.style.display = "block";		
	})
	vpanel.addEventListener("mouseenter", function () {
		contents.style.display = "block";
	});
	vpanel.addEventListener("mouseleave", function () {
		contents.style.display = "none";
	});

	contents.style.display = "none";
	p.style.margin = "0";
	p.style.marginTop = "1em";
	p.style.marginBottom = "0.5em";
	p.innerHTML = "Versions";
	
	var links = document.querySelectorAll("link[rel][href]");
	for (var i=0, l=links.length; i<l; i++) {
		var link = links[i],
			li, a;
		if (link.rel == "alternate") {
			append_link(versionlinks, link.href, link.title || link.href);
		} else if(link.rel == "search") {
			append_link(searchlinks, link.href, link.title || link.href);			
		}
	}



})