#!/usr/bin/env python

import sh
import cgi
import os
from urlparse import urlparse


env = os.environ
fs = cgi.FieldStorage()

docroot = env.get("DOCUMENT_ROOT")
ref = env.get("HTTP_REFERER")
src = urlparse(ref)
fp = os.path.join(docroot, src.path.lstrip("/"))

sh.cp(["-r", "/home/pi/.etherdump", fp])
ret = sh.etherdump(["pull", "--pub", ".", "--all"], _cwd=fp)

# etherdump index --templatepath . --template index.template.html *.meta.json > index.html
ret = sh.etherdump(["index", "--templatepath", ".", "--template", "index.template.html", "*.meta.json", ">", "index.html"], shell=True, _cwd=fp)

print "Location: {0}".format(ref)
print
