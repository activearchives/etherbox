#!/bin/bash

docroot=$DOCUMENT_ROOT
# ref=$HTTP_REFERER
# eventually make the path based on ref, but for now hardcode it...
path=$docroot/etherdump


cp -r /home/pi/.etherdump $path
cd $path
etherdump pull --pub . --all --css /include/etherdump.css --script /include/versions.js
etherdump index --templatepath /var/www --template index.etherdump.html --output index.html *.meta.json

echo Location: /etherdump
echo
